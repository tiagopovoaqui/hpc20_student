# Report Lab04

> Tiago Póvoa Quinteiro

## Introduction

Dans ce laboratoire, on va explorer perf et valgrind.



Pour le tri de liste, j'utilise toujours un tri casier. La plage de valeur sera configurée à:

>  MAX_RAND 1000000000

## Perf

### Stat

#### Array

```
 Performance counter stats for './sort array 1000000':

         67'822.80 msec task-clock                #    1.000 CPUs utilized          
               193      context-switches          #    0.003 K/sec                  
                 3      cpu-migrations            #    0.000 K/sec                  
             2'003      page-faults               #    0.030 K/sec                  
   263'336'953'967      cycles                    #    3.883 GHz                    
   204'131'245'834      instructions              #    0.78  insn per cycle         
    15'837'113'315      branches                  #  233.507 M/sec                  
        16'559'984      branch-misses             #    0.10% of all branches        

      67.825084656 seconds time elapsed

      67.799297000 seconds user
       0.023999000 seconds sys
```

#### List

```
 Performance counter stats for './sort list 1000000':

     2'524.06 msec task-clock                #    1.000 CPUs utilized          
                 6      context-switches          #    0.002 K/sec                  
                 0      cpu-migrations            #    0.000 K/sec                  
         1'960'982      page-faults               #    0.777 M/sec                  
     9'807'497'671      cycles                    #    3.886 GHz                    
    10'418'213'797      instructions              #    1.06  insn per cycle         
     2'789'623'100      branches                  # 1105.211 M/sec                  
         4'993'218      branch-misses             #    0.18% of all branches        

       2.524437209 seconds time elapsed

       1.272045000 seconds user
       1.252044000 seconds sys
```

#### Résultats

On a une vue d'ensemble de l'exécution. On peut noter qu'on a 0.10% et 0.18% ce qui semble être relativement peu. 

### Record

On va record plusieurs events. Ceux que j'ai décidé de suivre sont les suivants:

* Cycles
* cpu-clock
* faults
* cache-misses
* branch-misses
* migrations
* cs: context switch

`sudo perf record -e cycles,cpu-clock,faults,cache-misses,branch-misses,migrations,cs ./sort array 1000000`

#### Array

Sur les cycles, on voit qu'on passe la majorité du temps dans __memmove_avx_unaligned_erms

Probablement il s'agit de l'initialisation. 

![array general](./img/array_general.png)

Si on explore, même s'il n'est pas très significatif, mergeSort.part.0, on a quelques indices au sujet d'une fonction de comparaison.

![array mergesort](./img/array_in_mergesort.png)

Du coup, on va voir les **branch misses** pour essayer de voir où on a des problèmes:

![array mergesort](./img/array-branch-misses.png)

On voit qu'effectivement ce qui pose le plus de problème et cette portion de code:

```c
            while (index != start) {
                data[index] = data[index - 1];
                index--;
            }
            data[start] = value;
```

Elle est responsable de la fusion des listes. On devra voir plus tard comment améliorer ça.

-----

Quant aux **faults**, elles sont à 95% dans array_init qui est chargé d'initialiser l'array (et donc allouer la mémoire). Donc pas une information intéressante selon moi. 

#### List

En commençant par les cycles, cette fois c'est list_sort qui prend effectivement le plus de temps.

![list general](./img/list_general.png)

On a beaucoup d'instructions de jumps (branches). On va donc aller explorer plus en détails cette aspect. 

![list sort](./img/list_in_list_sort.png)

En fouillant un peu, voici la fonction la plus lourde: 

> évalué pour chacun des critères, c'est toujours celle qui apparaît le plus souvent

![list sort](./img/list_faults.png)

Selon mes observations, la partie la plus chaude du code est celle qui consiste à réecrire les occurences dans la liste.

```c
    for(uint64_t i = 0; i < ptr_count_arr[pos]; ++i) {
      list_ptr->data = pos;
      list_ptr = list_ptr->next;
    }
```

## Valgrind

### Lexique

- `Ir`: I cache reads (instructions executed)
- `I1mr`: I1 cache read misses (instruction wasn't in I1 cache but was in L2)
- `I2mr`: L2 cache instruction read misses (instruction wasn't in I1 or L2 cache, had to be fetched from memory)
- `Dr`: D cache reads (memory reads)
- `D1mr`: D1 cache read misses (data location not in D1 cache, but in L2)
- `D2mr`: L2 cache data read misses (location not in D1 or L2)
- `Dw`: D cache writes (memory writes)
- `D1mw`: D1 cache write misses (location not in D1 cache, but in L2)
- `D2mw`: L2 cache data write misses (location not in D1 or L2)

### Array

`valgrind --tool=callgrind --simulate-cache=yes ./sort array 100000`



```
==5111== Callgrind, a call-graph generating cache profiler
==5111== Copyright (C) 2002-2017, and GNU GPL'd, by Josef Weidendorfer et al.
==5111== Using Valgrind-3.15.0 and LibVEX; rerun with -h for copyright info
==5111== Command: ./sort array 100000
==5111== 
--5111-- warning: L3 cache found, using its data for the LL simulation.
==5111== For interactive control, run 'callgrind_control -h'.
==5111== 
==5111== Events    : Ir Dr Dw I1mr D1mr D1mw ILmr DLmr DLmw
==5111== Collected : 2082973195 633734243 629136876 991 274910264 259845 973 2085 13071
==5111== 
==5111== I   refs:      2,082,973,195
==5111== I1  misses:              991
==5111== LLi misses:              973
==5111== I1  miss rate:          0.00%
==5111== LLi miss rate:          0.00%
==5111== 
==5111== D   refs:      1,262,871,119  (633,734,243 rd + 629,136,876 wr)
==5111== D1  misses:      275,170,109  (274,910,264 rd +     259,845 wr)
==5111== LLd misses:           15,156  (      2,085 rd +      13,071 wr)
==5111== D1  miss rate:          21.8% (       43.4%   +         0.0%  )
==5111== LLd miss rate:           0.0% (        0.0%   +         0.0%  )
==5111== 
==5111== LL refs:         275,171,100  (274,911,255 rd +     259,845 wr)
==5111== LL misses:            16,129  (      3,058 rd +      13,071 wr)
==5111== LL miss rate:            0.0% (        0.0%   +         0.0%  )
```

### List

```
==4410== Callgrind, a call-graph generating cache profiler
==4410== Copyright (C) 2002-2017, and GNU GPL'd, by Josef Weidendorfer et al.
==4410== Using Valgrind-3.15.0 and LibVEX; rerun with -h for copyright info
==4410== Command: ./sort list 100000
==4410== 
--4410-- warning: L3 cache found, using its data for the LL simulation.
==4410== For interactive control, run 'callgrind_control -h'.
==4410== 
==4410== Events    : Ir Dr Dw I1mr D1mr D1mw ILmr DLmr DLmw
==4410== Collected : 6040100443 1008350588 5612985 988 125102656 234537 976 125060678 200540
==4410== 
==4410== I   refs:      6,040,100,443
==4410== I1  misses:              988
==4410== LLi misses:              976
==4410== I1  miss rate:          0.00%
==4410== LLi miss rate:          0.00%
==4410== 
==4410== D   refs:      1,013,963,573  (1,008,350,588 rd + 5,612,985 wr)
==4410== D1  misses:      125,337,193  (  125,102,656 rd +   234,537 wr)
==4410== LLd misses:      125,261,218  (  125,060,678 rd +   200,540 wr)
==4410== D1  miss rate:          12.4% (         12.4%   +       4.2%  )
==4410== LLd miss rate:          12.4% (         12.4%   +       3.6%  )
==4410== 
==4410== LL refs:         125,338,181  (  125,103,644 rd +   234,537 wr)
==4410== LL misses:       125,262,194  (  125,061,654 rd +   200,540 wr)
==4410== LL miss rate:            1.8% (          1.8%   +       3.6%  )

```

### Résultats

On peut ensuite appeler ouvrir plus en détails avec `callgrind_annotate --auto=yes`

### Memcheck

J'ai finalement testé avec memcheck, dans le doute.

```
valgrind --tool=memcheck --leak-check=yes ./sort array 100000
```

> All heap blocks were freed -- no leaks are possible

Donc ça devrait être correct. 

Et j'ai pu remarquer une memory leak dans mon tri casier. Je ne libérais pas la mémoire du tableau annexe de comptage d'occurences. 

## Optimisations

Il y a quelques optimisations théoriques qu'on peut faire. 

Pour le **merge_sort** sur l'array, on pourrait mieux exploiter la cache avec une optimisation particulière: arrêter de faire des appels récursifs quand le sous-tableau atteint une taille suffisamment petite. 

``` 
On modern computers, locality of reference can be of paramount importance in software optimization, because multilevel memory hierarchies are used. Cache-aware versions of the merge sort algorithm, whose operations have been specifically chosen to minimize the movement of pages in and out of a machine's memory cache, have been proposed. For example, the tiled merge sort algorithm stops partitioning subarrays when subarrays of size S are reached, where S is the number of data items fitting into a CPU's cache. Each of these subarrays is sorted with an in-place sorting algorithm such as insertion sort, to discourage memory swaps, and normal merge sort is then completed in the standard recursive fashion. This algorithm has demonstrated better performance[example needed] on machines that benefit from cache optimization. (LaMarca & Ladner 1997)

Kronrod (1969) suggested an alternative version of merge sort that uses constant additional space. This algorithm was later refined. (Katajainen, Pasanen & Teuhola 1996)

Also, many applications of external sorting use a form of merge sorting where the input get split up to a higher number of sublists, ideally to a number for which merging them still makes the currently processed set of pages fit into main memory.
```

Depuis cette source: (https://en.wikipedia.org/wiki/Merge_sort#Optimizing_merge_sort)

Il faudrait calculer, avec notre type (64 bits) et la taille d'une cache (256Ko, 1Mo et 8Mo sur ma machine par exemple) la taille effective S suggérée ci-dessus.  256 Ko / 64 bits = **32000**. Et ainsi comme mentionné, appliquer un tri en place (tri d'insertion). 

----

Pour le **counting sort**, le problème majeur vient du tableau annexe de comptage. 

Selon la distribution aléatoire des éléments, on peut se retrouver avec un tableau majoritairement creux:

> 0: 0, 1: 28, 2: 0, 3:0 .... n:0

Dans ce cas, itérer sur ces cases vides constitue une pénalité énorme. De plus, si la plage de valeur est large, on peut se retrouver avec une complexité pire qu'un tri O(N²) classique.

Une piste pourrait être d'adapter l'algorithme vers un bucket sort qui est similaire mais utilise des tables de hashages avec des fonctions de hashages adaptées à mettre ensemble des valeurs proches.

Une alternative serait le radix sort qui emprunte des concepts similaires.

