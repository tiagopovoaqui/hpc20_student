#include "list_util.h"
#include <time.h>

#define MAX_RAND 1000000000

// Forward declaration
/**
 * Utility function to allocate a single node
 * @return list_element * - Don't forge to free it
 */
struct list_element * create_node();

/* Allocate "len" linked elements and initialize them
 * with random data.
 * Return list head */
struct list_element *list_init(size_t len) {
  srand((unsigned)time(NULL));

  node* head = create_node();
  node* cur = head;

  for (size_t i = 0; i < len - 1; ++i) {
    cur->next = create_node();
    cur = cur->next;
  }

  return head;
}

/* Liberate list memory */
void list_clear(struct list_element *head) {
  node* cur;
  while(head != NULL) {
    cur = head->next; // we keep the next
    free(head); // we free the current who should not me null
    head = cur; // we go over the next
  }
}

/* Arrange a list in increasing order of value */
// Implementation: Counting sort
// We have a side array who stores the occurences of the values
void list_sort(struct list_element *head) {
  node * list_ptr = head;

  // We allocate an array to store the counts
  uint64_t* ptr_count_arr = calloc(MAX_RAND, sizeof(uint64_t));

  while(list_ptr != NULL) {
    ptr_count_arr[list_ptr->data] += 1; // We counted one more elem
    list_ptr = list_ptr->next; // Next
  }

  list_ptr = head; // Reset pos
  uint64_t pos = 0;

  while(list_ptr != NULL) {

    for(uint64_t i = 0; i < ptr_count_arr[pos]; ++i) {
      list_ptr->data = pos;
      list_ptr = list_ptr->next;
    }

    ++pos;
  }

  free(ptr_count_arr);
}

struct list_element * create_node() {
  node* cur = malloc(sizeof(struct list_element));
  cur->data = rand() % MAX_RAND;
  cur->next = NULL;
  return cur;
}
