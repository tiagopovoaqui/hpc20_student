#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <stdbool.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/stat.h>

#ifndef CLIENT_FILE
#define CLIENT_FILE "client_data.bin"
#endif

#ifndef FILE_SIZE
#define FILE_SIZE (1 << 30) /* 1 GB */
#endif

#ifndef MAX_SIZE
#define MAX_SIZE 4 * (1 << 20) /* 4 MB */
#endif

#ifndef BUF_SIZE
#define BUF_SIZE FILE_SIZE > MAX_SIZE ? MAX_SIZE : FILE_SIZE
#endif

static void client_recv(const int sock, FILE* file)
{
    ssize_t ret;
    ssize_t retwritten;
    uint8_t buf[BUF_SIZE] = { 0 };
    // Uncomment this if you wish to use dynamic alloc.
    // uint8_t* buf = malloc(BUF_SIZE * sizeof(uint8_t));

    for (size_t i = 0; i <= FILE_SIZE; i += ret) {
        /* receive from server */
        if ((ret = recv(sock, &buf, BUF_SIZE, MSG_WAITALL)) < 0) {
            perror("recv() error");
            exit(EXIT_FAILURE);
        }

        if (ret < 0) {
            perror("listen() error");
            exit(EXIT_FAILURE);
        }

        if (ret == 0) {
          break;
        }

        /* write to file */
        retwritten = fwrite(&buf, sizeof(uint8_t), ret, file);

        if (ret != retwritten) {
            fprintf(stderr, "[%s] Error: %zu bytes written (expected %zu)", __func__, retwritten, ret);
            exit(EXIT_FAILURE);
        }

    }

    printf("[%s] written %d bytes\n", __func__, FILE_SIZE);

    // Uncomment this if you wish to use dynamic alloc.
    // free(buf);
}

void client_start(const char *ipv4_srv, unsigned short port_srv)
{
    FILE* file;
    int sock;
    struct sockaddr_in addr_srv;

    /* file init */
    file = fopen(CLIENT_FILE, "w");
    if (file < 0) {
        perror("Error opening file");
        exit(EXIT_FAILURE);
    }

    /* socket init */
    if ((sock = socket(PF_INET, SOCK_STREAM, IPPROTO_TCP)) < 0) {
        perror("Error creating client socket");
        exit(EXIT_FAILURE);
    }

    memset(&addr_srv, 0, sizeof(addr_srv));
    addr_srv.sin_family = AF_INET;
    addr_srv.sin_addr.s_addr = inet_addr(ipv4_srv);
    addr_srv.sin_port = htons(port_srv);

    if (connect(sock, (struct sockaddr *) &addr_srv, sizeof(addr_srv)) < 0) {
        perror("connect() error");
        exit(EXIT_FAILURE);
    }

    /* connected */
    client_recv(sock, file);

    close(sock);
    fclose(file);
}
