# Report Lab02
> Tiago Póvoa Quinteiro

## Introduction

In this laboratory, we'll try to improve a small C program to observe I/O bottlenecks.

The behavior is the following:
1. reads from the disk
2. +1 on all the bytes
3. sends with sockets

### How to run

Cleaning: `$ make clean`

Compile and run: `make run FILE_SIZE=$`

Clean, compile, run and verify: `make all`

## Identifying the problem

As we started the lab, I tried to launch the code. At this time, I had a random rate of failure
in which the program wouldn't even finish. So, the first step was finding the root cause of the problem.

```cpp
ret = read(file, &buf, 1);
```

The code just above comes from `server.c` file.
We can see that we're calling read (which uses a syscall behind the scenes)
with a size of one. And we're doing it one byte at a time.

It does make a strong penality on the performance.

> syscall saves CPU registers before making the system call, restores
> the registers upon return from the system call

As stated in the man pages, we can see that syscalls are heavy operations.

If we look other portions of `server.c` and `client.c`, we have the same
problem with the calls of `send()` and `recv()`.

## First step

To improve it substantially we have to replace this `1` value with a buffer.
So first we declare a buffer size:

```cpp
#define BUF_SIZE 8192
```

And then we can make less syscalls (read, send, recv). As in this example:

```cpp
ret = read(file, &buf, BUF_SIZE);
```

and then, from a painfull and lenghty execution, we finally can make it work

```SHELL
./main
[srv_start] 127.0.0.1 connected
[srv_send] sent 8192 bytes
[client_recv] written 8192 bytes
[time_report] Report:
[time_report] -------
[time_report] size: 8192 bytes
[time_report] time: 0 sec and 2206300 ns
[time_report] rate: 3713003.671305 bytes/sec

```

## Finding the right buffer size

### Page size

A first approach would be to adapt the buffer size according to the page size of the system.

For this we can:

```cpp
printf("The page size for this system is %ld bytes.\n",
    sysconf(_SC_PAGESIZE));
```

or

```SHELL
$ getconf PAGE_SIZE
```

We get the value `4096`. And here we have an example of execution:

```SHELL
./main
[srv_start] 127.0.0.1 connected
[srv_send] sent 1073741824 bytes
[client_recv] written 1073741824 bytes
[time_report] Report:
[time_report] -------
[time_report] size: 1073741824 bytes
[time_report] time: 553 sec and 514141159 ns
[time_report] rate: 1939863.400331 bytes/sec
```

So we have a rate of 1.9 Mb/sec. Not very satisfying, but okay.

### Buffer size = File size

Now let's say, instead of having just "one page sized" buffer we go all the way up to the file size.

```
./main
[srv_start] 127.0.0.1 connected
[client_recv] written 1073741824 bytes
[srv_send] sent 1073741824 bytes
[time_report] Report:
[time_report] -------
[time_report] size: 1073741824 bytes
[time_report] time: 5 sec and 69861240 ns
[time_report] rate: 211789193.662429 bytes/sec
```

Okay, now we're at 200Mb/sec.
The improvement is great, but we have to consider that we're allocating on the stack 1GB.

It may cause problems if we go further. So I'll add:

```cpp
#define BUF_SIZE FILE_SIZE > 4194304 ? 4194304 : FILE_SIZE
```

> We could improve this by checking the capacity of the current system we're running on.

### Memory consumption

The bigger the better, right? Well, maybe not by much. Let's experiment.

> FILE_SIZE = 1GB
> ALWAYS have done make clean before running

| Buffer size | Rate (MB/s) | Execution time (s) |
| --- | --- | --- |
| 4096 (Page size) | 1.93 | 553 |
| 8192 (L3 cache size) | 2.34 | 457 |
| 32768 | 8.27 | 129 |
| 65536 | 22.57 | 47 |
| 262144 | 116.77 | 9 |
| 1048576 | 235.91 | 4.55 |
| 4194304 | 345.67 | 3.84 |
| 16777216 | 220.91 | 4.86 |
| 67108864 | 262.24 | 4.94 |
| 268435456 | 218.85 | 4.90 |
| 1GB (BUF_SIZE = FILE_SIZE) | 210.32 | 5 |

Here we have some raw measurements, in the next section we are going to analyze it.

### Data

Let's dive into the data we measured in the previous section.
Here we can see the evolution of the time spent. At roughly 0.2 MB we arrive at something nice.

![1](./img/1.png)

If we zoom in the data to see more closely the range above 262144, we
now can clearly see that we don't have any real benefit going up.

![2](./img/2.png)

Same thing for the rate.

![3](./img/3.png)

In the end, the better buffer size seems to be 4194304 bytes (~ 4MB).

Having so much memory to work with might be less efficient due to all the page miss we'll get.

## Using FLAGS

### Send and Recv flags

For this set of tests, I tried to see if there was an improvement by using
some flags. In send(), I used `MSG_MORE` and in recv() `MSG_WAITALL`.

| Buffer size | Rate (MB/s) | Execution time (s) |
| --- | --- | --- |
| 4194304 | 303.45 | 3.53 |
| 16777216 | 247.11 | 4.34 |
| 67108864 | 262.25 | 4.95 |
| 1/8 GB | 416.47 | 3.40 |
| 0.5 GB | 416.47 | 2.57 |
| BUF_SIZE=FILE_SIZE | 421.29 | 2.54 |

As we can see, we only benefit from it with huge buffer sizes. In particular when
it's close to the FILE_SIZE.

While using those flags, I had a few errors. It might be interesting at times.
But the performance benefit is small. And the necessity
of having similar sizes (BUF_SIZE=FILE_SIZE) makes it less practical.

### Compilation FLAGS

This time, I added some CFLAGS to the compilation:

`-g -O3 -march=native -std=gnu11`

With the following I had a little improvement. Maybe, in part because I removed the profiling (-p).

With those, I was between 2.80 - 3.4 seconds. (buffer size ~4MB)

## Code changes

### Replacing open, read and write

While reading about I/O in C, I found out that replacing read and write by fread and fwrite could improve the performance in POSIX systems. 

So I tried to replace it both in server and client. In the bright side, now fopen returns a `FILE *` which is a bit prettier than a int.

Since I tested it home (and not on the test machine in class), I could not evaluate it with my previous measures. 

With a Ubuntu VM 4GB I went from 27-16s down to 14-6s. Nice improvement. 

Another interesting thing, this system doesn't allow me to allocate a lot of memory (in class I could make 1GB arrays). So it reinforces my opinion that we should not use that much memory for a tiny program like this one if not necessary. I still could edit it and allocate on the heap with a malloc... 

### Malloc

Again, I'm testing this step at home in a different machine which is suboptimal.

As I mentioned previously, it cannot allocate a lot more than 8MB on the stack. So for testing purposes I changed the code to a malloc version.

I have not noticed such a great improvement. I was at 10-8s (14-6s previously). Maybe it would perform better outside a Virtual Machine. But it could also mean that having this much memory with all the cache miss and the page faults doesn't improve by much. To get a better perspective on this, we could explore deeper by trying on very large size files.

## Recap about our testing system

The computer has 16 GB of ram.

Here is a recap about the pages.

| Command       |         Value      |
| ------------- | ------------- |
| getconf PAGE_SIZE | 4096 bytes |
| getconf _PHYS_PAGES | 4093090 (this * pagesize ~= 16GB)|
| getconf _AVPHYS_PAGES | 2739155 |

With the command dmidecode I also obtained the following:

* cache L1 256 kB
* cache L2 1024 kB
* cache L3  8192 kB

## Other considerations

I tried to use pthreads in order to read from the disk and send at the same time.

I've implemented it the naive way just to see if there was a huge gap.
I did not experiment any particular improvements. But maybe with a really good implementation
we could gain a few ms.

## Question of the assignement

> Entre la première execution du programme et les suivantes sur le même fichier, que remarquez-vous et
> comment l’expliquez-vous ? Est-ce que la taille du fichier a une importance ? Donnez votre
> analyse dans le rapport

With the following:

`$ free -h`

We can observe that the system puts a lot of memory in cache. So we can assume that it
really improves our I/O operations.

## Conclusion

* The program could simply not work without a buffer.
* Improving the size  of the buffer above a certain threshold doesn't seem to help much.
* Using fread and fwrite is better than read and write.
* The C compilation flags helped gain some seconds 