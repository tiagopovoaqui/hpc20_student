# Report Lab07

## Optimisations de compilation

> Tiago Póvoa Quinteiro

## Objectif du laboratoire

Mettre en évidence le comportement et les optimisations du compilateur en fonction du code fourni

### Liens utiles

https://gcc.gnu.org/onlinedocs/gcc-9.1.0/gcc/Optimize-Options.html#Optimize-Options

## Exemple 1 - Série géométrique

Pour ce premier exemple, nous allons traiter une série géométrique.

Rappel: https://en.wikipedia.org/wiki/Geometric_progression#Geometric_series

Lien vers le code sur compiler explorer: https://godbolt.org/z/XVbbo8

```c
int geometric(int firstTerm, int numberOfTerms, int reason) {
    int result = 0;
    int product = 1;

    for (int i = 0; i < numberOfTerms; ++i) {
        result += firstTerm * product;
        product *= reason;
    }

    return result;
}
```

La première version du code supposée naïve va effectuer la série en additionnant les termes comme suit:

Avec le premier terme=2, nombre de terme=4, raison=5:

`2 + 2 * 5 + 2 * 5² + 2 * 5³ = 312`

On a donc une boucle qui va additionner et multiplier plusieurs valeurs à chaque itération.

### Optimisation manuelle

```c
int geometric_formula(int firstTerm, int numberOfTerms, int reason) {
    int product = 1;

    for (int i = 0; i < numberOfTerms; ++i) {
        product *= reason;
    }

    return firstTerm * (1 - product) / (1 - reason);
}

int geometric_formula_pow(int firstTerm, int numberOfTerms, int reason) {

    return firstTerm * (1 - (int) pow(reason, numberOfTerms)) / (1 - reason);
}
```

Pour l'optimisation manuelle, il y a deux possibilités similaires avec des trade of discutables.

Parlons de la seconde version: sachant qu'une série géométrique peut être calculée par une formule, on peut directement en évaluer le résultat. `(a * 1 - r^N)/(1 - r)` 

Le soucis c'est d'avoir un overhead d'aller appeler une librairie, qui de plus va devoir convertir les types vu que la fonction est faite sur des double. 

La première version  va simplement calculer la raison dans une boucle. Attention ici on assume qu'on a des entrées entières ce qui simplifie le calcule de l'exposant. Elle pourrait ouvrir la porte à d'autres optimisations.

Note: ces deux versions doivent utiliser une division qui est plus lourde que des additions. Donc elle pourrait être moins performante si les valeurs à calculer sont petites.

### Optimisation compilateur

Lien vers le code: https://godbolt.org/z/48Au5q

Le plus gros saut d'optimisation s'est fait en utilisant le flag `-O1`.

Il a simplifié la multiplication en une seule opération. Il voit qu'elle est immédiatement réutilisé et donc il la garde juste dans un registre en enlevant deux moves.

Par analogie, on peut voir que l'addition aussi a été simplifié d'une manière similaire.

Concrètement, il a vu que ces valeurs étaient réutilisés tel quel sans être réaffectées et donc il move une seule fois product et result dans les registres et effectue des opérations en place.

Malheureusement, mon code est un poile complexe pour l'idée de ce laboratoire. Pour les exemples qui suivront je vais prendre des cas plus spécifiques. 

## Exemple 2 - Normalization

Lien vers le code: https://godbolt.org/z/ZAbfFD

On va tenter d'optimiser une fonction qui normalise un tableau. Une méthode trivial est de diviser chaque élément par la longueur. Et ici en plus on va rendre positif les éléments négatifs.

```c
void normalize(float* tab_ptr, int len) {
    for(int i = 0; i < LEN; ++i) {
        tab_ptr[i] /= len;
    }

    for(int i = 0; i < LEN; ++i) {
        if(tab_ptr[i] < 0.) {
            tab_ptr[i] *= -1;
        }
    }
}
```

### Optimisation manuelle

On va commencer par appliquer du loop jamming / fusion. On va du coup passer de 5 * 2 instructions à juste 4 pour la partie boucle. On peut aussi remplacer la partie pour rendre la valeur positive par un appel à la fonction de valeur absolue `fabs()`.

```c
void normalize_jammed(float* tab_ptr, int len) {
    for(int i = 0; i < LEN; ++i) {
        tab_ptr[i] = fabs(tab_ptr[i]);
        tab_ptr[i] /= len;
    }
}
```

### Optimisation compilateur

En effet, si on applique -O3, il va fusionner les boucles. Sur compiler explorer la seconde boucle ne sera plus en couleur car pas utilisée. 

À partir de -O2 il va considérablement réduire le traitement des division et de fabs. Mais pourtant il ne fera pas le loop jamming. Donc ça reste utile de le faire à la main. D'ailleurs le code manuelle est un meilleur candidat à l'optimisation, il produit largement moins d'instructions de toute façon.

Lien vers le code: https://godbolt.org/z/aLQuJQ

## Exemple 3 - Factorielle

Lors d'un appel récursif, les compilateurs modernes vont appliquer l'optimisation de la **tail recursion**.

Notamment, on peut recycler la fonction sur la stack plutôt que d'empiler une nouvelle à chaque appel.

Lien vers le code: https://godbolt.org/z/ee08DK

Dans le code fournit, j'ai tenté 4 approches. 

Celle de base, `factorial` est la fonction comme on peut l'implémenter de la manière la plus mathématique possible.

`factorial_loop` et `factorial goto` sont deux alternatives de l'idée de rester dans la même fonction et de faire une démarche itérative à la place.

`factorial_tail` est une variante de la première, qui utilise toujours les appels récursifs mais devrait utiliser la récursion de queue vu qu'il y a rien à faire dans la fonction une fois l'appel final effectué. Pas même l'évaluation d'une expression.

Pour le code assembleur généré, on reste à des ordres similaire. Ce qui est intéressant, c'est d'observer quelle fonction, une fois le niveau d'optimisation du compilateur augmenté, va se réduire le plus.

|                | -O0          | -O1          | -O2  | -O3  |
| -------------- | ------------ | ------------ | ---- | ---- |
| factorial      | 16 avec call | 11 avec call | 7    | 7    |
| factorial_tail | 18 avec call | 10 avec call | 8    | 8    |
| factorial_loop | 19           | 8            | 8    | 8    |
| factoria_goto  | 16           | 8            | 8    | 8    |

Et il faut évidemment comptabiliser le fait que en -O0, loop et goto utilisent des jmp mais pas de call. 

À partir de **-O2**, plus aucune fonction n'a de **call** (d'appel récursif). Ils deviennent alors tous des variantes qui font approximativement la même chose. 

Notamment, goto et tail sont totalement identique en assembleur. Les deux autres ont de petites différences test au lieu de cmp par exemple.

## Conclusion

Ma première fonction était trop compliquée pour vraiment pouvoir démontrer une optimisation précise. Probablement plusieurs rentrent en jeu, et c'est difficile de les distinguer.

La seconde a montré assez facilement ce qui était supposé.

La troisième était peut-être un peu trop développée et fournie, j'ai fouillé un peu le sujet car étant amateur de programmation fonctionnelle, ça m'intéressait de comparer plusieurs approches.

### Vu en cours

J'ai juste noté ça pour éviter de les faire par erreur.

* INLINE EXPANSION OR INLINING
* DEAD CODE ELIMINATION
* CONSTANT PROPAGATION OR FOLDING
* STRENGTH REDUCTION: échanger des opérations par des plus rapide
* UNROLLING
* MEMORY ALIASING

