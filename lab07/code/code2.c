// Type your code here, or load an example.
#include <stdio.h>
#define LEN 10

void normalize(float* tab_ptr, int len) {
    for(int i = 0; i < LEN; ++i) {
        tab_ptr[i] /= len;
    }

    for(int i = 0; i < LEN; ++i) {
        if(tab_ptr[i] < 0.) {
            tab_ptr[i] *= -1;
        }
    }
}

int main() {
    
    float tab[LEN] = {1,-2,3,-4,5,-12, 14, -16, 18, -20};

    normalize(tab, LEN);

    for(int i = 0; i < LEN; ++i) {
        printf("%f ", tab[i]);
    }

    return 0;
}