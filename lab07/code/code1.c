#include <stdio.h>
#include <math.h>

int geometric(int firstTerm, int numberOfTerms, int reason) {
    int result = 0;
    int product = 1;

    for (int i = 0; i < numberOfTerms; ++i) {
        result += firstTerm * product;
        product *= reason;
    }

    return result;
}

int geometric_formula(int firstTerm, int numberOfTerms, int reason) {
    int product = 1;

    for (int i = 0; i < numberOfTerms; ++i) {
        product *= reason;
    }

    return firstTerm * (1 - product) / (1 - reason);
}

int geometric_formula_pow(const int firstTerm, const int numberOfTerms, const int reason) {

    return firstTerm * (1 - (int) pow(reason, numberOfTerms)) / (1 - reason);
}

int main() {

    printf("Result 1 of (2,4,5): %i\n", geometric(2, 4, 5));
    printf("Result 2 of (2,4,5): %i\n", geometric(2, 4, 5));
    printf("Result 3 of (2,4,5): %i\n", geometric_formula_pow(2, 4, 5));

    return 0;
}

