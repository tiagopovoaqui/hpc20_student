

# Report Lab05

> Tiago Povoa Quinteiro

## Objectif

Trouver s'il y a des pertes de performances venant des fonctions **rgb_to_grayscale, gaussian_filter et sobel_filter**

## Gaussian_filter

En réutilisant ce qui a été appris au laboratoire précédent, j'ai bêtement refais un petit perf.

```
sudo perf record -e cycles,cpu-clock,faults,cache-misses,branch-misses,migrations,cs ./sobel images/medalion.png images/edge_medalion.png
```

```
sudo perf report
```

![](./img/1.png)

On voit qu'on a des cache miss dans gaussian_filter. On continue l'exploration

![](./img/2.png)

Et cette fois si on met en évidence la ligne de code `pix_acc += kernel...`

En regardant de plus près la fonction:

```C
void gaussian_filter(const struct img_t *img, struct img_t *res_img, const uint16_t *kernel)
{

    const uint16_t gauss_ponderation = 16;

    for (size_t x = 0; x < img->width; x++) {
        for (size_t y = 0; y < img->height; y++) {
            uint16_t pix_acc = 0;

            if (x < GAUSSIAN_KERNEL_SIZE/2 ||
                x >= img->width - GAUSSIAN_KERNEL_SIZE/2 ||
                y < GAUSSIAN_KERNEL_SIZE/2 ||
                y >= img->height - GAUSSIAN_KERNEL_SIZE/2) {
                    res_img->data[y * res_img->width + x] = img->data[y * img->width + x];
                    continue;
            }

            for (size_t ky = 0; ky < GAUSSIAN_KERNEL_SIZE; ky++) {
                for (size_t kx = 0; kx < GAUSSIAN_KERNEL_SIZE; kx++) {
                    pix_acc += kernel[ky * GAUSSIAN_KERNEL_SIZE + kx] *
                               img->data[(y - 1 + ky) * img->width + (x - 1 + kx)];
                }
            }

            res_img->data[y * res_img->width + x] = pix_acc / gauss_ponderation;
        }
    }
}
```

Ainsi, le problème à l'air de venir de cette double boucle.

```C
for (size_t kx = 0; kx < GAUSSIAN_KERNEL_SIZE; kx++) {
    pix_acc += kernel[ky * GAUSSIAN_KERNEL_SIZE + kx] *
        img->data[(y - 1 + ky) * img->width + (x - 1 + kx)];
}
```

Beaucoup de cache miss: probablement un problème lier à la localité spatiale de la mémoire. 

J'ai tenté de changer l'ordre des boucles kx et ky.

En étudiant de plus près le code, on voit qu'on va lire la matrice (l'image) colonne par colonne, et pour chaque ligne à ligne. Ce qui semble étrange par rapport à notre compréhenssion de la spatialité de la mémoire.

```c
for (size_t x = 0; x < img->width; x++) { 
    for (size_t y = 0; y < img->height; y++) {
```

À remplacer par

```C
    for (size_t y = 0; y < img->height; y++) { 
        for (size_t x = 0; x < img->width; x++) {
```



## Sobel_filter

Avec les changements précédents suggérés, on devrait pouvoir améliorer suffisament gaussian_filter pour que ce ne soit plus le principal bottle_neck.

En appliquant une logiquement équivalent, voyons si on peut observer des choses similaires ici.

En y regardant de plus près, on a effectivement le même problème.

## rgb_to_grayscale

Je n'ai pas trouvé de choses particulières à changer. Même en comparant à un code "classique" supposé faire la même chose.

https://rosettacode.org/wiki/Grayscale_image#C