/* 06.05.2020 - Sydney Hauke - HPC - REDS - HEIG-VD */

#include <stdlib.h>
#include <stdio.h>
#include <stddef.h>
#include <time.h>

#include <xmmintrin.h> // SSE
#include <emmintrin.h> // SSE2
#include <pmmintrin.h> // SSE3
#include <tmmintrin.h> // SSSE3
#include <smmintrin.h> // SSE4.1
#include <nmmintrin.h> // SSE4.2

#include "image.h"
#include "ifs.h"
#include "random.h"

#define INTRINSICS_WIDTH 4

#define TRANSFORM_SIZE 6

/* Sierpinski triangle */
#define NB_ST_TRANSFORMS 3
float ST_TRANSFORMS[NB_ST_TRANSFORMS][TRANSFORM_SIZE] = {
    {
        0.5f,
        0.0f,
        0.0f,
        0.0f,
        0.5f,
        0.0f,
    },
    {
        0.5f,
        0.0f,
        0.5f,
        0.0f,
        0.5f,
        0.0f,
    },
    {
        0.5f,
        0.0f,
        0.25f,
        0.0f,
        0.5f,
        0.5f,
    }};

/* Barnsley's fern */
#define NB_BF_TRANSFORMS 4
float BF_TRANSFORMS[NB_BF_TRANSFORMS][TRANSFORM_SIZE] = {
    {
        0.0f,
        0.0f,
        0.0f,
        0.0f,
        0.16f,
        0.0f,
    },
    {
        0.2f,
        -0.26f,
        0.0f,
        0.23f,
        0.22f,
        1.6f,
    },
    {
        -0.15f,
        0.28f,
        0.0f,
        0.26f,
        0.24f,
        0.44f,
    },
    {
        0.85f,
        0.04f,
        0.0f,
        -0.04f,
        0.85f,
        1.6f,
    }};

struct ifs_t
{
    float width;
    float height;

    float center_x;
    float center_y;

    size_t nb_transforms;
    float *transforms;
};

/* Sierpinski triangle IFS */
const struct ifs_t st_ifs = {
    .width = 1.0f,
    .height = 1.0f,

    .center_x = 0.5f,
    .center_y = 0.5f,

    .nb_transforms = NB_ST_TRANSFORMS,
    .transforms = (float *)ST_TRANSFORMS,
};

/* Barnley's fern IFS */
const struct ifs_t bf_ifs = {
    .width = 6.0f,
    .height = 10.0f,

    .center_x = 0.0f,
    .center_y = 4.5f,

    .nb_transforms = NB_BF_TRANSFORMS,
    .transforms = (float *)BF_TRANSFORMS,
};

// old
// static void affine_transform(float *x, float *y, const float transform[6]);
static void affine_transform_4(__m128 *x, __m128 *y, const float transforms[TRANSFORM_SIZE][4]);

static size_t __ifs(struct img_t *img, const struct ifs_t *ifs, size_t passes, size_t width, size_t height);

void ifs(char *pathname, size_t passes, size_t min_width)
{
    const struct ifs_t *i = &bf_ifs;
    struct img_t *fractal_img;
    size_t width, height;
    float aspect_ratio;

    /* Fractals have a specific aspect ratio. The resulting image
     * must have the same aspect ratio as well */
    aspect_ratio = i->height / i->width;
    width = min_width;
    height = width * aspect_ratio;

    fractal_img = allocate_image(width, height, COMPONENT_GRAYSCALE);

    /* Generate fractal image */
    size_t points_drawn = __ifs(fractal_img, i, passes, width, height);
    printf("Number of points drawn : %lu\n", points_drawn);

    save_image(pathname, fractal_img);
    printf("Fractal saved as %s (%lu, %lu)\n", pathname, width, height);

    free_image(fractal_img);
}

static size_t __ifs(struct img_t *img, const struct ifs_t *ifs, size_t passes, size_t width, size_t height)
{
    /* TODO : do multiple instances of this algorithm so that the number of points generated 
     * per second increases. Use SIMD instructions. */

    /* This is the real number of iterations we do to draw the fractal */
    size_t count_points = width * height * passes;
    
    // old
    /* We start from the origin point */
    // float p_x = 0.0f;
    // float p_y = 0.0f;
    // struct xorshift32_state rand_state; // random number generator

    __m128 points_x = _mm_set_ps1(0.0f);
    __m128 points_y = _mm_set_ps1(0.0f);

    srand(time(NULL));

    /* Choose a random starting state for the random number generator */
    __m128i all_state_a = _mm_set_epi32(rand(), rand(), rand(), rand());

    // Bounds of the image. Was useful for tests that we don't need anymore
    // Now we just need min bounds at the end
    // __m128 bound_x_max = _mm_set_ps1(ifs->center_x + ifs->width / 2);
    __m128 bound_x_min = _mm_set_ps1(ifs->center_x - ifs->width / 2);
    // __m128 bound_y_max = _mm_set_ps1(ifs->center_y + ifs->height / 2);
    __m128 bound_y_min = _mm_set_ps1(ifs->center_y - ifs->height / 2);

    uint32_t rand_idx_extracted[INTRINSICS_WIDTH];
    __m128i TRANSFORM_SIZE_BLOCK = _mm_set1_epi32(TRANSFORM_SIZE);

    // This array allows to prepare data to pass in affine_transform_4
    float transforms[TRANSFORM_SIZE][INTRINSICS_WIDTH];

    /// Last part position calculation
    __m128 _ifs_height = _mm_set1_ps(ifs->height);
    __m128 _ifs_width = _mm_set1_ps(ifs->width);
    __m128 __height = _mm_set1_ps(height);
    __m128 __width = _mm_set1_ps(width);
    __m128 __height_min_one = _mm_set1_ps(height - 1);

    __m128 __x;
    __m128 __y;

    uint32_t final_points[INTRINSICS_WIDTH];

    for (size_t iterations = count_points; iterations != 0; iterations--) {
        /* Randomly choose an affine transform */
        xorshift128(&all_state_a);

        // There is no modulus operation on SIMD integers :(
        _mm_store_si128((__m128i *)rand_idx_extracted, all_state_a);
        for (uint8_t i = 0; i < INTRINSICS_WIDTH; ++i) {
            rand_idx_extracted[i] %= ifs->nb_transforms;
        }

        // Start - affine
        __m128i _transform_pos = _mm_load_si128((__m128i *) rand_idx_extracted);
        _transform_pos = _mm_mul_epu32(_transform_pos, TRANSFORM_SIZE_BLOCK);
        _mm_store_si128((__m128i *)rand_idx_extracted, _transform_pos);

        for (uint8_t i = 0; i < TRANSFORM_SIZE; ++i) {
            for (uint8_t j = 0; j < INTRINSICS_WIDTH; ++j) {
                transforms[i][j] = ifs->transforms[rand_idx_extracted[j] + i];
            }
        }

        /* Apply choosen affine transform */
        affine_transform_4(&points_x, &points_y, (const float(*)[INTRINSICS_WIDTH]) & transforms);

        // (height-1-(int32_t)((p_y-bottom)/ifs->height*height))*width
        __y = _mm_sub_ps(points_y, /*bottom*/ bound_y_min);
        __y = _mm_div_ps(__y, _ifs_height);
        __y = _mm_mul_ps(__y, __height);
        __y = _mm_floor_ps(__y);
        __y = _mm_sub_ps(__height_min_one, __y);
        __y = _mm_mul_ps(__y, __width);

        // (int32_t)((p_x-left)/ifs->width*width)
        __x = _mm_sub_ps(points_x, /*left*/ bound_x_min);
        __x = _mm_div_ps(__x, _ifs_width);
        __x = _mm_mul_ps(__x, __width);
        __x = _mm_floor_ps(__x);

        // Last addition, conversion to integer and store it
        _mm_storeu_si128((__m128i *) final_points, _mm_cvtps_epi32(_mm_add_ps(__x, __y)));

        for (uint8_t k = 0; k < 4; ++k) {
            /* If point lies in the image, save and colour it in white */
            img->data[final_points[k] ] = UINT8_MAX;
        }
    }

    return count_points  * INTRINSICS_WIDTH;
}

/*
// old
static void affine_transform(float *x, float *y, const float transform[TRANSFORM_SIZE])
{
    float xtmp = *x;
    float ytmp = *y;
    *x = transform[0] * xtmp + transform[1] * ytmp + transform[2];
    *y = transform[3] * xtmp + transform[4] * ytmp + transform[5];
}
*/

static void affine_transform_4(__m128 *x, __m128 *y, const float transforms[TRANSFORM_SIZE][INTRINSICS_WIDTH]) {

    // transform[0] * xtmp
    __m128 transform_line = _mm_load_ps(transforms[0]);
    __m128 a = _mm_mul_ps(*x, transform_line);

    // transform[1] * ytmp
    transform_line = _mm_load_ps(transforms[1]);
    __m128 b = _mm_mul_ps(*y, transform_line);

    // (transform[0] * xtmp) + (transform[1] * ytmp)
    a = _mm_add_ps(a, b);

    // (transform[0] * xtmp + transform[1] * ytmp) + transform[2];
    transform_line = _mm_load_ps(transforms[2]);
    __m128 x_tmp = _mm_add_ps(a, transform_line);

    // second line

    // transform[3] * xtmp
    transform_line = _mm_load_ps(transforms[3]);
    a = _mm_mul_ps(*x, transform_line);

    // transform[4] * ytmp
    transform_line = _mm_load_ps(transforms[4]);
    b = _mm_mul_ps(*y, transform_line);

    // (transform[3] * xtmp) + (transform[4] * ytmp)
    a = _mm_add_ps(a, b);

    // (transform[3] * xtmp + transform[4] * ytmp) + transform[5]
    transform_line = _mm_load_ps(transforms[5]);
    *y = _mm_add_ps(a, transform_line); // Final value y

    *x = x_tmp; // Final value x
}