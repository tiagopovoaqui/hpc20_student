/* 06.05.2020 - Sydney Hauke - HPC - REDS - HEIG-VD */

#include "random.h"

uint32_t xorshift32(struct xorshift32_state *state)
{
    uint32_t x = state->a;
    x ^= x << 13;
    x ^= x >> 17;
    x ^= x << 5;
    return state->a = x;
}

void xorshift128 (__m128i* all_state) {
    // xorshift - Start of black magic wizardry
    __m128i result = *all_state;

    // x ^= x << 13
    __m128i to_shift = _mm_slli_epi32(result, 13);
    result = _mm_xor_si128(result, to_shift);

    // x ^= x >> 17
    to_shift = _mm_srai_epi32(result, 17);
    result = _mm_xor_si128(result, to_shift);

    // x ^= x << 5
    to_shift = _mm_slli_epi32(result, 5);
    result = _mm_xor_si128(result, to_shift);
    // xorshift - End

    *all_state = result;
}