#pragma once

#include <stdint.h>


#include <xmmintrin.h> // SSE
#include <emmintrin.h> //SSE2
#include <tmmintrin.h> // SSSE3


struct xorshift32_state {
    uint32_t a;
};

uint32_t xorshift32(struct xorshift32_state *state);

void xorshift128 (__m128i* all_state);
