# Report Lab08 - Fractales IFS (SIMD)

## Objectif de ce laboratoire 

Appliquer une parallélisation par instruction SIMD sur un générateur de fractales.

Lien de référence: https://software.intel.com/sites/landingpage/IntrinsicsGuide/#expand=103&techs=SSE,SSE2,SSE3,SSSE3,SSE4_1,SSE4_2

## Première exécution

Pour avoir une référence pour plus tard, voici la première exécution. 

```
./ifs-bm
2020-05-13 10:01:07
Running ./ifs-bm
Run on (8 X 3900 MHz CPU s)
CPU Caches:
  L1 Data 32 KiB (x4)
  L1 Instruction 32 KiB (x4)
  L2 Unified 256 KiB (x4)
  L3 Unified 8192 KiB (x1)
Load Average: 0.22, 0.45, 0.55
***WARNING*** CPU scaling is enabled, the benchmark real time measurements may be noisy and will incur extra overhead.
------------------------------------------------------------------------------
Benchmark                    Time             CPU   Iterations UserCounters...
------------------------------------------------------------------------------
IFSFixture/ifs_test    2340616 us      2340595 us            1 points/s=106.768M/s total_points=249.9M
```

Time: 2340616 us

1 points/s=106.768M/s

total_points=249.9M

## Informations

### Imports

En fonction des opérations souhaitées, il faut importer les headers suivants:

```c
<xmmintrin.h> SSE
<emmintrin.h> SSE2
<pmmintrin.h> SSE3
<tmmintrin.h> SSSE3
<smmintrin.h> SSE4.1
<nmmintrin.h> SSE4.2
<ammintrin.h> SSE4A
```

source: https://stackoverflow.com/questions/11228855/header-files-for-x86-simd-intrinsics

### Options de compilation

`-march=haswell -msse -msse2 -msse3  -msse4`

Afin de bénéficier des opérations SIMD, il faut ajouter les options adéquates à la compilation.

De plus, j'ai ajouté l'architecture de mon cpu (haswell) pour être au plus proche de ma cible.

## Adaptation SIMD

###  xorshift32

La première partie à adapter, et vraissemblablement la plus simple est la fonction xorshift32:

```c
uint32_t xorshift32(struct xorshift32_state *state) {
    uint32_t x = state->a;
    x ^= x << 13;
    x ^= x >> 17;
    x ^= x << 5;
    return state->a = x;
}
```

Depuis une donnée aléatoire sur un entier 32 bits non-signé, on effectue des décalages et des ou-exclusifs.

On va donc adapter le tout sur le type entier 128 bits `__m128i`

```c
void xorshift128 (__m128i* all_state) {
    // xorshift
    __m128i result = *all_state;

    // x ^= x << 13
    __m128i to_shift = _mm_slli_epi32(result, 13);
    result = _mm_xor_si128(result, to_shift);

    // x ^= x >> 17
    to_shift = _mm_srai_epi32(result, 17);
    result = _mm_xor_si128(result, to_shift);

    // x ^= x << 5
    to_shift = _mm_slli_epi32(result, 5);
    result = _mm_xor_si128(result, to_shift);
    // xorshift - End

    *all_state = result;
}
```

### Préparation des données de transform

Au retour de la fonction précédente, on doit appliquer un modulo. Malheureusement, ce n'est pas une opération SIMD qui existe. 

```c
size_t rand_idx = xorshift32(&rand_state) % ifs->nb_transforms;
```

Il existe probablement une astuce pour des valeurs particulières. Pour le moment on la fait en série (dans une boucle). C'est un point qu'on pourrait améliorer. 

```c
/* Apply choosen affine transform */
affine_transform(&p_x, &p_y, &ifs->transforms[rand_idx*TRANSFORM_SIZE]);
```

Quant à la transform choisie, elle va poser des difficultés. Dans la fonction `affine_transform` on a besoin de charger des cases spécifiques de ce tableau annexe - mais cette fois, pour chaque tableau annexe sélectionné.

#### Version 1

```c
float transforms[6][4];
// We need the correct transform each of the 4 random values we calculated
for (uint8_t i = 0; i < 4; ++i) {
    float *transforms_tab_ptr = &ifs->transforms[_mm_extract_epi32(transform_pos, i)];

    for (uint8_t j = 0; j < 6; ++j) {
        transforms[j][i] = transforms_tab_ptr[j];
    }
}
```

Dans transforms, on veut que chaque ligne contienne les quatres valeurs de transform du même indice. Ainsi on aurait le quadruplet de valeurs 0, ou 1, etc.

#### Version 2

La version précédente de préparation des transforms pose un problème: on parcours ce tableau par colonnes. Afin de profiter au mieux de la cache, on veut itérer sur des zones contiguës. 

```c
for (uint8_t i = 0; i < TRANSFORM_SIZE; ++i) {
    for (uint8_t j = 0; j < INTRINSICS_WIDTH; ++j) {
        transforms[i][j] = ifs->transforms[rand_idx_extracted[j] + i];
    }
}
```

En inversant l'ordre de parcours on a fait un bon d'opérations par secondes assez conséquent.

### Affine_transform

```c
static void affine_transform(float *x, float *y, const float transform[TRANSFORM_SIZE]) {
    *x = transform[0] * *x + transform[1] * *y + transform[2];
    *y = transform[3] * *x + transform[4] * *y + transform[5];
}
```

Cette fois, on doit donc préparer un vecteur de 4 points pour x, et pour y. On passe donc de `float` à `__m128`. 

```c
static void affine_transform_4(__m128 *x, __m128 *y, const float transforms[TRANSFORM_SIZE][INTRINSICS_WIDTH]) {

    // transform[0] * xtmp
    __m128 transform_line = _mm_load_ps(transforms[0]);
    __m128 a = _mm_mul_ps(*x, transform_line);

    // transform[1] * ytmp
    transform_line = _mm_load_ps(transforms[1]);
    __m128 b = _mm_mul_ps(*y, transform_line);

    // (transform[0] * xtmp) + (transform[1] * ytmp)
    a = _mm_add_ps(a, b);

    // (transform[0] * xtmp + transform[1] * ytmp) + transform[2];
    transform_line = _mm_load_ps(transforms[2]);
    __m128 x_tmp = _mm_add_ps(a, transform_line);

    // second line

    // transform[3] * xtmp
    transform_line = _mm_load_ps(transforms[3]);
    a = _mm_mul_ps(*x, transform_line);

    // transform[4] * ytmp
    transform_line = _mm_load_ps(transforms[4]);
    b = _mm_mul_ps(*y, transform_line);

    // (transform[3] * xtmp) + (transform[4] * ytmp)
    a = _mm_add_ps(a, b);

    // (transform[3] * xtmp + transform[4] * ytmp) + transform[5]
    transform_line = _mm_load_ps(transforms[5]);
    *y = _mm_add_ps(a, transform_line); // Final value y

    *x = x_tmp; // Final value x
}
```

Comme on peut voir ci-dessus, on peut effectuer toutes les opérations en ayant qu'à charger la bonne ligne de transforms. Les indices restent les mêmes, donc on ne sacrifie même pas la lisibilité du code. 

### Calcul final du point

```c
if (p_x < ifs->center_x+ifs->width/2 && p_x > ifs->center_x-ifs->width/2 && 
            p_y < ifs->center_y+ifs->height/2 && p_y > ifs->center_y-ifs->height/2) {
    
    float left = ifs->center_x-ifs->width/2;
    float bottom = ifs->center_y-ifs->height/2;

    /* If point lies in the image, save and colour it in white */
    img->data[(height-1-(int32_t)((p_y-bottom)/ifs->height*height))*width + 
              (int32_t)((p_x-left)/ifs->width*width)] = UINT8_MAX;
}
```

Dans cet extrait ci-dessus, on voit qu'on teste si la composante x et y se trouvent bien sur l'image. 

Au départ, j'avais tenté d'appliquer des masques. Pour n'ajouter que les points qui se trouvent en effet sur l'image, et ne rien faire sinon.

C'était fastidieux et ça ne semblait même pas fonctionner. Le fait qu'au final tous les points passent ou ne passent pas en fonction de petits ajustements m'a intrigué. En revenant au commit initial du labo8, et en retirant la condition, j'ai en effet remarqué qu'on pouvait tout à fait se passer de ce test. Les points sont de base déjà corrects.

```c
/* If point lies in the image, save and colour it in white */
img->data[(height-1-(int32_t)((p_y-bottom)/ifs->height*height))*width + 
          (int32_t)((p_x-left)/ifs->width*width)] = UINT8_MAX;
```

Ainsi, on se retrouve uniquement avec cette ligne ci-dessus. qui deviendra:

```c
// (height-1-(int32_t)((p_y-bottom)/ifs->height*height))*width
__y = _mm_sub_ps(points_y, /*bottom*/ bound_y_min);
__y = _mm_div_ps(__y, _ifs_height);
__y = _mm_mul_ps(__y, __height);
__y = _mm_floor_ps(__y);
__y = _mm_sub_ps(__height_min_one, __y);
__y = _mm_mul_ps(__y, __width);

// (int32_t)((p_x-left)/ifs->width*width)
__x = _mm_sub_ps(points_x, /*left*/ bound_x_min);
__x = _mm_div_ps(__x, _ifs_width);
__x = _mm_mul_ps(__x, __width);
__x = _mm_floor_ps(__x);

// Last addition, conversion to integer and store it
_mm_storeu_si128((__m128i *) final_points, _mm_cvtps_epi32(_mm_add_ps(__x, __y)));
```

On effectue toutes les opérations du calcul du point dans deux vecteurs temporaires. Note: pour éviter d'avoir deux opérations de `cast` à effectuer (`_mm_cvtps_epi32`), on `floor` les résultats intermédiaires et on `cast` uniquement à la fin: au moment du store. 

```c
for (uint8_t k = 0; k < 4; ++k) {
	/* If point lies in the image, save and colour it in white */
	img->data[final_points[k] ] = UINT8_MAX;
}
```

Et finalement, on ajoute ces points à l'image.

## Résultats

Rappel (version point par point):

```
------------------------------------------------------------------------------
Benchmark                    Time             CPU   Iterations UserCounters...
------------------------------------------------------------------------------
IFSFixture/ifs_test    2340616 us      2340595 us            1 points/s=106.768M/s total_points=249.9M
```

Version finale:

```
IFSFixture/ifs_test    5132715 us      5132649 us            1 points/s=194.753M/s total_points=999.6M
```

Comment interpréter ces résultats?

Tout d'abord, on génère en effet 4 fois plus de points. Donc sans surprise le total augmente en conséquence.

On passe de 2.34s à 5.13s. Si on se réfère à la documentation de Intel, on voit qu'on a une certaine latence et un throughput pour les opérations SIMD. Les plus coûteuses étant les extract, store, load, etc.

On pourrait un peu réduire ce temps en limitant ce type d'opération. Dans cette version, on a déjà évité tous les extract et les unaligned loads. Cependant, on a malgré tout beaucoup de dépendance de données. Le compilateur peut faire quelques optimisations en réordonant, mais c'est malgré tout limité.

La métrique la plus importante est celle du nombre de points par seconde: 

**106. M/s -> 194. M/s**

Malgré la perte de vitesse générale et le throughput pas forcément optimal, on parvient malgré tout à générer presque deux fois plus de points par seconde. 

Considérant que, selon la documentation intel, le cpu skylake (un cpu plus récent) a une latency plus basse que le haswell (le cpu sur lequel j'ai testé), on pourrait même avoir un débit encore plus important.