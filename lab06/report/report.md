# Lab06

> Tiago Povoa Quinteiro

## Résultats de base

Comme demandé dans le laboratoire, il faut tester sur un serveur distant afin d'avoir une configuration standard.

Voici les résultats sans avoir effectué aucun changement. Ils vont servir de base de comparaison.

| Benchmark                                                    | Time (us) | CPU (us) | Iterations |
| ------------------------------------------------------------ | --------- | -------- | ---------- |
| RGB2GSFixture/rgb2gs_test/images/medalion.png:4593121        | 3364      | 3364     | 208        |
| RGB2GSFixture/rgb2gs_test/images/half-life.png:4593100       | 12'070    | 12070    | 58         |
| RGB2GSFixture/rgb2gs_test/images/stars.png:4593083           | 98840     | 98839    | 6          |
| FiltersFixture/gaussian_filter_test/images/medalion.png:4593121 | 8948      | 8948     | 78         |
| FiltersFixture/gaussian_filter_test/images/half-life.png:4593100 | 94'742    | 94740    | 7          |
| FiltersFixture/gaussian_filter_test/images/stars.png:4593083 | 1'089'140 | 1089126  | 1          |
| FiltersFixture/sobel_filter_test/images/medalion.png:4593121 | 15'418    | 15418    | 45         |
| FiltersFixture/sobel_filter_test/images/half-life.png:4593100 | 106'250   | 106248   | 7          |
| FiltersFixture/sobel_filter_test/images/stars.png:4593083    | 1'288'324 | 1288307  | 1          |

## Améliorations

### Lab05

En effectuant simplement les changements suggerés au laboratoire précédent:

| Benchmark                                                    | Time (us)  | CPU (us) | Iterations |
| ------------------------------------------------------------ | ---------- | -------- | ---------- |
| RGB2GSFixture/rgb2gs_test/images/medalion.png:4593121        | 3360       | 3359     | 208        |
| RGB2GSFixture/rgb2gs_test/images/half-life.png:4593100       | 12079      | 12077    | 58         |
| RGB2GSFixture/rgb2gs_test/images/stars.png:4593083           | 98800      | 98775    | 6          |
| FiltersFixture/gaussian_filter_test/images/medalion.png:4593121 | 7379       | 7378     | 95         |
| FiltersFixture/gaussian_filter_test/images/half-life.png:4593100 | **29196**  | 29191    | 24         |
| FiltersFixture/gaussian_filter_test/images/stars.png:4593083 | **258623** | 258582   | 3          |
| FiltersFixture/sobel_filter_test/images/medalion.png:4593121 | 16152      | 16148    | 43         |
| FiltersFixture/sobel_filter_test/images/half-life.png:4593100 | **59621**  | 59620    | 12         |
| FiltersFixture/sobel_filter_test/images/stars.png:4593083    | **612032** | 611896   | 1          |

Pour gaussian_filter:

* half-life: 94'742 -> **29'196**  -69%
* stars: 1'089'140 -> **258'623** -76%

Pour Sobel_filter:

* half-life: 106'250 -> **59'621** -43%
* stars: 1'288'324 -> **612'032** -52%

Rien qu'en ayant mit les boucles dans le bon ordre on a un gain conséquent en temps.

Sur les autres parties, il n'y a pas eu l'air d'y avoir une grosse influence.

### Exploration

#### Option de compilation

Comme vu en cours, on peut utiliser `-march=native` pour mieux cibler l'architecture vers laquelle on veut compiler. On a gagné des us sur RGB2GSFixture. Sur le reste, la différence ne semble pas significative. 

| Benchmark                                              | Time (us) | CPU (us) | Iterations |
| ------------------------------------------------------ | --------- | -------- | ---------- |
| RGB2GSFixture/rgb2gs_test/images/medalion.png:4593121  | 3079      | 3079     | 227        |
| RGB2GSFixture/rgb2gs_test/images/half-life.png:4593100 | 11045     | 11045    | 63         |
| RGB2GSFixture/rgb2gs_test/images/stars.png:4593083     | 90216     | 90215    | 7          |

On pourrait en changer d'autre, mais c'est explicitement demandé de ne pas le faire dans la consigne.

#### Retirer un appel sqrt

Bien qu'aujourd'hui le problème d'effectuer une racine carrée n'est plus si important comme il a pu l'être jadis, la retirer peut améliorer quelque peu les performances. De plus, on évite un appel de librairie.

J'ai remplacé donc: *(ligne 106)*

```C
res_img->data[y * res_img->width + x] = 
    sqrt(Gx*Gx + Gy*Gy) > SOBEL_BINARY_THRESHOLD * SOBEL_BINARY_THRESHOLD ? UINT8_MAX : 0;
```

Par:

```C
res_img->data[y * res_img->width + x] = 
                Gx*Gx + Gy*Gy > SOBEL_BINARY_THRESHOLD_SQUARED ? UINT8_MAX : 0;
```

Où SOBEL_BINARY_THRESHOLD_SQUARED est une valeur précalculée.

| Benchmark                                                    | Time (us) | CPU (us) | Iterations |
| ------------------------------------------------------------ | --------- | -------- | ---------- |
| FiltersFixture/sobel_filter_test/images/medalion.png:4593121 | 12730     | 12730    | 55         |
| FiltersFixture/sobel_filter_test/images/half-life.png:4593100 | 49845     | 49845    | 14         |
| FiltersFixture/sobel_filter_test/images/stars.png:4593083    | 431075    | 431069   | 2          |

### Retirer des tests

Lorsque l'on itère sur toute l'image, on fait systèmatiquement un test inutile pour la majorité de l'image sauf les bords.

*Ligne 60*

```c
if (x < HALF_GAUSSIAN_KERNEL_SIZE ||
    x >= img->width - HALF_GAUSSIAN_KERNEL_SIZE ||
    y < HALF_GAUSSIAN_KERNEL_SIZE ||
    y >= img->height - HALF_GAUSSIAN_KERNEL_SIZE) {
    res_img->data[y * res_img->width + x] = img->data[y * img->width + x];
    continue;
}
```

On pourrait retirer ce test si on itère uniquement sur l'intérieur de l'image et qu'on définit à part l'affectation brute.

En testant concrètement, je n'ai eu que des améliorations sur les toutes petites images. Sur les autres j'ai eu une dégradation. Du coup je ne l'ai pas implementé.

### Réutilisation des valeurs calculées

Dans l'extrait de code suivant, on voit qu'on accède consécutivement aux éléments du kernel et de img->data.

*Ligne 64*

```C
for (size_t ky = 0; ky < GAUSSIAN_KERNEL_SIZE; ky++) {
    for (size_t kx = 0; kx < GAUSSIAN_KERNEL_SIZE; kx++) {
        pix_acc += kernel[ky * GAUSSIAN_KERNEL_SIZE + kx] *
            img->data[(y - 1 + ky) * img->width + (x - 1 + kx)];
    }
}
```

Si on imagine que nos opérations consistent à effectuer un calcul glissant de 3 par 3 sur l'image d'origine, on peut s'imaginer qu'on a certaines réutilisations de valeurs. La colonne centrale est décalée à gauche, la colonne de droite est décalée au centre.

Pour pouvoir tirer partie de ça, j'ai déconstruit la boucle en 9 opérations singulières. En dehors de la peine que c'est pour les yeux à regarder, l'intérêt est de pouvoir stocker le résultat de ces opérations et de les réutiliser à l'itération suivante.

| Benchmark                                                    | Time (us) | CPU (us) | Iterations |
| ------------------------------------------------------------ | --------- | -------- | ---------- |
| FiltersFixture/**gaussian_filter_test**/images/medalion.png:4593121 | 6691      | 6691     | 105        |
| FiltersFixture/**gaussian_filter_test**/images/half-life.png:4593100 | 24067     | 24067    | 29         |
| FiltersFixture/**gaussian_filter_test**/images/stars.png:4593083 | 198830    | 198830   | 3          |

Pour le fichier stars, on est passé de 250k à 198k us. 

On peut, par analogie, faire la même chose pour le filter sobel:

| Benchmark                                                    | Time (us) | CPU (us) | Iterations |
| ------------------------------------------------------------ | --------- | -------- | ---------- |
| FiltersFixture/**sobel_filter_test**/images/medalion.png:4593121 | 10597     | 10597    | 66         |
| FiltersFixture/**sobel_filter_test**/images/half-life.png:4593100 | 38225     | 38225    | 18         |
| FiltersFixture/**sobel_filter_test**/images/stars.png:4593083 | 315577    | 315577   | 2          |

## Rappel d'utilisation

Cette section est uniquement là pour garder une trace des utilisations utiles du programme.

`./sobel-bm`

`./sobel images/medalion.png images/edge_medalion.png`

