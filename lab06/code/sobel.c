#include <stdlib.h>
#include <stdio.h>
#include <math.h>

#include "image.h"
#include "sobel.h"

#define GAUSSIAN_KERNEL_SIZE    3
#define SOBEL_KERNEL_SIZE       3

#define SOBEL_BINARY_THRESHOLD  150  // in the range 0 to uint8_max (255)
#define SOBEL_BINARY_THRESHOLD_SQUARED 22500 // This value MUST be the squared of 150 

const int16_t sobel_v_kernel[SOBEL_KERNEL_SIZE*SOBEL_KERNEL_SIZE] = {
    -1, -2, -1,
     0,  0,  0,
     1,  2,  1,
};

const int16_t sobel_h_kernel[SOBEL_KERNEL_SIZE*SOBEL_KERNEL_SIZE] = {
    -1,  0,  1,
    -2,  0,  2,
    -1,  0,  1,
};

const uint16_t gauss_kernel[GAUSSIAN_KERNEL_SIZE*GAUSSIAN_KERNEL_SIZE] = {
    1, 2, 1,
    2, 4, 2,
    1, 2, 1,
};

void rgb_to_grayscale(const struct img_t *img, struct img_t *result)
{
    size_t index = 0;

    for (size_t y = 0; y < result->height; y++) {
        for (size_t x = 0; x < result->width; x++) {
            result->data[y * result->width + x] =
                FACTOR_R * img->data[index + R_OFFSET] +
                FACTOR_G * img->data[index + G_OFFSET] +
                FACTOR_B * img->data[index + B_OFFSET];

            index += img->components;
        }
    }
}

void gaussian_filter(const struct img_t *img, struct img_t *res_img, const uint16_t *kernel)
{

    const uint16_t gauss_ponderation = 16;
    const size_t HALF_GAUSSIAN_KERNEL_SIZE = GAUSSIAN_KERNEL_SIZE/2;
    uint16_t pix_acc = 0;

    // On prépare les variables.
    // x et y = 1 car on skip 0 de toute façon
    uint16_t a = img->data[(1 - 1 ) * img->width + (1 - 1)];
    uint16_t b = img->data[(1 - 1 ) * img->width + (1)];
    uint16_t c = 0;
    uint16_t d = img->data[(1) * img->width + (1 - 1)];
    uint16_t e = img->data[(1) * img->width + (1)];
    uint16_t f = 0;    
    uint16_t g = img->data[(1 + 1) * img->width + (1 - 1)];
    uint16_t h = img->data[(1 + 1) * img->width + (1)];
    uint16_t i = 0;

    for (size_t y = 0; y < img->height; y++) {
        for (size_t x = 0; x < img->width; x++) {
            pix_acc = 0;

            // Skip les
            if (x < HALF_GAUSSIAN_KERNEL_SIZE ||
                x >= img->width - HALF_GAUSSIAN_KERNEL_SIZE ||
                y < HALF_GAUSSIAN_KERNEL_SIZE ||
                y >= img->height - HALF_GAUSSIAN_KERNEL_SIZE) {
                    res_img->data[y * res_img->width + x] = img->data[y * img->width + x];
                    continue;
            }

            // éléments du bord. Les autres ont déjà été calculés
            c = img->data[(y - 1 ) * img->width + (x + 1)];
            f = img->data[(y) * img->width + (x + 1)];
            i = img->data[(y + 1) * img->width + (x + 1)];

            pix_acc = kernel[0] * a + kernel[3] * b + kernel[6] * c 
                + kernel[1] * d + kernel[4] * e + kernel[7] * f 
                + kernel[2] * g + kernel[5] * h + kernel[8] * i;

            // On recycle les valeurs.
            a = b;
            b = c;
            d = e;
            e = f;
            g = h;
            h = i;

            res_img->data[y * res_img->width + x] = pix_acc / gauss_ponderation;
        }

        // Reset en fin de ligne
        // Test pour éviter de déborder
        if (y != 0 && y != img->height) {
            a = img->data[(y - 1 ) * img->width + (1 - 1)];
            b = img->data[(y - 1 ) * img->width + (1)];
            d = img->data[(y) * img->width + (1 - 1)];
            e = img->data[(y) * img->width + (1)];
            g = img->data[(y + 1) * img->width + (1 - 1)];
            h = img->data[(y + 1) * img->width + (1)];
        }
    }
}


void sobel_filter(const struct img_t *img, struct img_t *res_img, 
                  const int16_t *v_kernel, const int16_t *h_kernel)
{
    uint16_t a = img->data[(1 - 1 ) * img->width + (1 - 1)];
    uint16_t b = img->data[(1 - 1 ) * img->width + (1)];
    uint16_t c = 0;
    uint16_t d = img->data[(1) * img->width + (1 - 1)];
    uint16_t e = img->data[(1) * img->width + (1)];
    uint16_t f = 0;    
    uint16_t g = img->data[(1 + 1) * img->width + (1 - 1)];
    uint16_t h = img->data[(1 + 1) * img->width + (1)];
    uint16_t i = 0;

    for (size_t y = 0; y < img->height; y++) {
        for (size_t x = 0; x < img->width; x++) {
            int16_t Gx = 0;
            int16_t Gy = 0;
            
            if (x < SOBEL_KERNEL_SIZE/2 ||
                x >= img->width - SOBEL_KERNEL_SIZE/2 ||
                y < SOBEL_KERNEL_SIZE/2 ||
                y >= img->height - SOBEL_KERNEL_SIZE/2) {
                    res_img->data[y * res_img->width + x] = img->data[y * img->width + x];
                    continue;
            }

            // éléments du bord. Les autres ont déjà été calculés
            c = img->data[(y - 1 ) * img->width + (x + 1)];
            f = img->data[(y) * img->width + (x + 1)];
            i = img->data[(y + 1) * img->width + (x + 1)];

            Gx = h_kernel[0] * a + h_kernel[1] * b +  h_kernel[2] * c
                + h_kernel[3] * d + h_kernel[4] * e + h_kernel[5] * f
                + h_kernel[6] * g + h_kernel[7] * h + h_kernel[8] * i;

            Gy = v_kernel[0] * a + v_kernel[1] * b + v_kernel[2] * c
                + v_kernel[3] * d + v_kernel[4] * e + v_kernel[5] * f
                + v_kernel[6] * g + v_kernel[7] * h + v_kernel[8] * i;

            res_img->data[y * res_img->width + x] = 
                Gx*Gx + Gy*Gy > SOBEL_BINARY_THRESHOLD_SQUARED ? UINT8_MAX : 0;

            // On recycle les valeurs.
            a = b;
            b = c;
            d = e;
            e = f;
            g = h;
            h = i;
        }

        // Reset en fin de ligne
        // Test pour éviter de déborder
        if (y != 0 && y != img->height) {
            a = img->data[(y - 1 ) * img->width + (1 - 1)];
            b = img->data[(y - 1 ) * img->width + (1)];
            d = img->data[(y) * img->width + (1 - 1)];
            e = img->data[(y) * img->width + (1)];
            g = img->data[(y + 1) * img->width + (1 - 1)];
            h = img->data[(y + 1) * img->width + (1)];
        }
    }
}

struct img_t *edge_detection(const struct img_t *input_img)
{
    struct img_t *res_img;
    struct img_t *gauss_img;
    struct img_t *gs_img;

    if (input_img->components < COMPONENT_RGB) {
        fprintf(stderr, "[%s] only accepts images with RGB(A) components", __func__);
        return NULL;
    }

    gs_img = allocate_image(input_img->width, input_img->height, COMPONENT_GRAYSCALE);
    gauss_img = allocate_image(gs_img->width, gs_img->height, gs_img->components);
    res_img = allocate_image(gs_img->width, gs_img->height, gs_img->components);

    rgb_to_grayscale(input_img, gs_img);
    gaussian_filter(gs_img, gauss_img, gauss_kernel);
    sobel_filter(gauss_img, res_img, sobel_v_kernel, sobel_h_kernel);

    free_image(gauss_img);
    free_image(gs_img);

    return res_img;
}