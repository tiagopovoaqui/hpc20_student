#include "array_util.h"
#include <time.h>

#define MAX_RAND 1000000000

// Forward declaration of some util methods
/**
 * Implements a merge sort. HEAVILY inspired by GfG
 * @param data the pointer of the start of an array you want to sort
 * @param l pos of the left
 * @param r pos of the right
 * @source https://www.geeksforgeeks.org/in-place-merge-sort/
 */
void mergeSort(uint64_t *data, size_t l, size_t r);

/**
 * Merge of the merge sort
 * @param data the pointer of the start
 * @param start start position
 * @param mid middle position
 * @param end final position
 */
void merge(uint64_t *data, size_t start, size_t mid, size_t end);


/* Allocate an array of size "len" and fill it
 * with random data.
 * Return the array pointer */
uint64_t *array_init(const size_t len) {
    srand((unsigned)time(NULL));

    uint64_t* ptr = malloc(len * sizeof(uint64_t));

    for(size_t i = 0; i < len; ++i) {
      ptr[i] = rand() % MAX_RAND;
    }

    return ptr;
}

/* Liberate array memory */
void array_clear(uint64_t *data) {
  free(data);
}

/* Arrange a array in increasing order of value */
void array_sort(uint64_t *data, const size_t len) {
  mergeSort(data, 0, len);
}

void mergeSort(uint64_t *data, size_t l, size_t r) {
    if (l < r) {
        int m = l + (r - l) / 2;

        // Sort first and second halves
        mergeSort(data, l, m);
        mergeSort(data, m + 1, r);

        merge(data, l, m, r);
    }
}

void merge(uint64_t *data, size_t start, size_t mid, size_t end) {
    size_t start2 = mid + 1;

    // If the direct merge is already sorted
    if (data[mid] <= data[start2]) {
        return;
    }

    // Two pointers to maintain start
    // of both arrays to merge
    while (start <= mid && start2 <= end) {

        // If element 1 is in right place
        if (data[start] <= data[start2]) {
            start++;
        }
        else {
            size_t value = data[start2];
            size_t index = start2;

            // Shift all the elements between element 1
            // element 2, right by 1.
            while (index != start) {
                data[index] = data[index - 1];
                index--;
            }
            data[start] = value;

            // Update all the pointers
            start++;
            mid++;
            start2++;
        }
    }
}
