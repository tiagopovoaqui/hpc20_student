#include <stdio.h>
#include <stdlib.h>
#include "benchmark/benchmark.h"

extern "C" {
#include "array_util.h"
#include "list_util.h"
}

#define DATA_LEN 1000000

static void FuncArray(benchmark::State& state) {
  uint64_t * array_ptr = array_init(DATA_LEN);

  // Perform setup here
  for (auto _ : state) {
    // This code gets timed
    array_sort(array_ptr, DATA_LEN);
  }

  array_clear(array_ptr);
}

static void FuncList(benchmark::State& state) {
  node * list_ptr = list_init(DATA_LEN);

  // Perform setup here
  for (auto _ : state) {
    // This code gets timed
    list_sort(list_ptr);
  }
  list_clear(list_ptr); // Clean mem
}

static void FuncStd(benchmark::State& state) {
  std::vector<u_int64_t> v(DATA_LEN);
  std::generate(v.begin(), v.end(), std::rand);

    // Perform setup here
  for (auto _ : state) {
    // This code gets timed
    std::sort(v.begin(), v.end());
  }
}

BENCHMARK(FuncStd);

BENCHMARK(FuncList);

BENCHMARK(FuncArray);

BENCHMARK_MAIN();
