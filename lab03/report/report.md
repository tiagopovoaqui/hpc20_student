# Report lab03

> Tiago Póvoa Quinteiro

### Documents

J'ai édité les documents `.xlsx` dans Google Sheets. S'il y a des soucis, voici les liens:

Benchmark results

https://drive.google.com/file/d/1h46zHwtBK8ZTvg6M5oX-x6gI0fDqkmLs/view?usp=sharing

Know your hardware

https://drive.google.com/file/d/17I2QgYZNO55eHqr1OlYy3l7WvB7gdMZU/view?usp=sharing

### Machine

J'ai utilisé la machine en classe sous Linux et ma machine personnel sur une machine virtuelle. J'ai eu quelques problèmes avec Google benchmark (pas possible de faire plus qu'un certain nombre d'itérations sans planter). 

*Je vais tâcher de faire un dual boot pour éviter cette situation difficile pour le prochain labo.*

### Algorithmes

Pour le tri de la liste, j'ai utilisé un tri casier car il me semblait intéressant.

* Il comporte une grave faiblesse: le tableau annexe qui sert à construire une sorte d'histogramme ne peut pas scale (Il faudrait une variable de comptage * l'ensemble des nombres du type)
* Il est probablement le plus rapide sur les entiers à condition d'avoir un min et un max raisonnable. 

Je l'ai testé avec un min et max de 0 à 1Mio. Ce serait un algorithme intéressante à implementer pour les types entiers si, dans une implémentation théorique, on stocke le min et max en tout temps d'un tableau.

Si ce min et max est plus bas qu'une certaine plage, il vaincra à faible coup de mémoire annexe n'importe quel autre tri.

En revanche, si cette plage est élevé, il faudra se rabattre sur un des grands classiques.

## 1

Remplir la première partie était relativement trivial. La seconde a demandé plus de recherches.

Sur la partie Gflop/s et Gop/s, j'ai eu des difficultées à trouver des informations. 

Gflop/s : Ghz * 1/(Cycles pour une op flotante) * nombre op par cycle * nombre de core

(pas sûr)

Gop/s: Basé sur des données théoriques fournies dans une documentation de Intel

## 2

Time, strace -tt et gprof ont donnés des résultats similaires en temps.

Ce qui a surtout changé était le niveau de détails (plus élevé avec gprof, car par fonction) et la mise en place (devoir exécuter une ligne de commande, contre modifier des options de compilation et linkage)

## 3

### 3a

> Question: Pourquoi l’une de ces deux fonctions est plus performante que l’autre ?

La seule différence subtile se trouve dans les deux boucles imbriqués. On itère sur m2, puis m1

```c
    for (size_t i = 0; i < m1->row; i++)
        for (size_t j = 0; j < m2->col; j++)
            for (size_t k = 0; k < m1->col; k++)
                m3->data[i][j] += m1->data[i][k] * m2->data[k][j];
```

et dans la version "xchg" on itère sur m1, puis m2

```c
    for (size_t i = 0; i < m1->row; i++)
        for (size_t k = 0; k < m1->col; k++)
            for (size_t j = 0; j < m2->col; j++)
                m3->data[i][j] += m1->data[i][k] * m2->data[k][j];
```

### 3b

Aucune observation particulière. Mes deux tris n'ont pas de "pire cas" ou "meilleur cas". Contrairement à des autres types de tri qui sont O(N) avec une liste déjà triée.

En revanche, si on réduit la plage des valeurs, comme dit précédement, le tri casier réduit de manière extravaguante son temps d'exécution.

### Optionnel

Le std:sort, testé sur un vector, va largement plus vite que mon implémentation de array et tri fusion.

De 70s à 1s.